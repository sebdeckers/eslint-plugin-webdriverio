# eslint-plugin-webdriverio 🤖

The `browser` and `$`/`$$` helpers are globally available in [WebdriverIO](http://webdriver.io/). This [ESLint](http://eslint.org/) plugin makes it easy to configure your WDIO project.

## Usage

There are several ways to [configure environments in ESLint](http://eslint.org/docs/user-guide/configuring#specifying-environments).

For example in combination with Mocha:

### Individual Source File

```js
/* eslint-env webdriverio/wdio, mocha */
```

### ESLint

`.eslintrc`:

```js
{
  "plugins": ["webdriverio"],
  "env": {
    "webdriverio/wdio": true,
    "mocha": true
  }
}
```

### StandardJS

`package.json`:

```json
{
  "standard": {
    "plugins": [
      "webdriverio"
    ],
    "env": [
      "mocha",
      "webdriverio/wdio"
    ]
  }
}
```
