module.exports = {
  environments: {
    wdio: {
      globals: {
        browser: false,
        $: false,
        $$: false
      }
    }
  }
}
